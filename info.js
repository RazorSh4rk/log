var info={
    timeOpened:new Date(),
    timezone:(new Date()).getTimezoneOffset()/60,

    pageon(){return window.location.pathname},
    referrer(){return document.referrer},
    previousSites(){return history.length},

    browserName(){return navigator.appName},
    browserEngine(){return navigator.product},
    browserVersion1a(){return navigator.appVersion},
    browserVersion1b(){return navigator.userAgent},
    browserLanguage(){return navigator.language},
    browserOnline(){return navigator.onLine},
    browserPlatform(){return navigator.platform},
    javaEnabled(){return navigator.javaEnabled()},
    dataCookiesEnabled(){return navigator.cookieEnabled},
    dataCookies1(){return document.cookie},
    dataCookies2(){return decodeURIComponent(document.cookie.split(";"))},
    dataStorage(){return localStorage},

    sizeScreenW(){return screen.width},
    sizeScreenH(){return screen.height},
    sizeDocW(){return document.width},
    sizeDocH(){return document.height},
    sizeInW(){return innerWidth},
    sizeInH(){return innerHeight},
    sizeAvailW(){return screen.availWidth},
    sizeAvailH(){return screen.availHeight},
    scrColorDepth(){return screen.colorDepth},
    scrPixelDepth(){return screen.pixelDepth},
}

function dumpInfo(){
    return '["' + info.timeOpened + '",'
    + '"' + info.timeZone + '",'

    + '"' + info.previousSites() + '",'

    + '"' + info.pageon() + '",'
    + '"' + info.referrer() + '",'
    + '"' + info.previousSites() + '",'

    + '"' + info.browserName() + '",'
    + '"' + info.browserEngine() + '",'
    + '"' + info.browserVersion1a() + '",'
    + '"' + info.browserVersion1b() + '",'
    + '"' + info.browserLanguage() + '",'
    + '"' + info.browserOnline() + '",'
    + '"' + info.browserPlatform() + '",'
    + '"java: ' + info.javaEnabled() + '",'
    + '"cookies: ' + info.dataCookiesEnabled() + '",'
    + '"' + info.dataCookies1() + '",'
    + '"' + info.dataCookies2() + '",'
    + '"' + info.dataStorage() + '",'

    + '"' + info.sizeScreenW() + '",'
    + '"' + info.sizeScreenH() + '",'
    + '"' + info.sizeDocW() + '",'
    + '"' + info.sizeDocH() + '",'
    + '"' + info.sizeInW() + '",'
    + '"' + info.sizeInH() + '",'
    + '"' + info.sizeAvailW() + '",'
    + '"' + info.sizeAvailH() + '",'
    + '"' + info.scrColorDepth() + '",'
    + '"' + info.scrPixelDepth() + '"]'
}